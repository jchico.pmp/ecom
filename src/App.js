import React from 'react';
import AddFriend from "./components/AddFriend";
import Friend from "./components/Friend";
// import logo from './logo.svg';
// import './App.css';


class App extends React.Component{
  state = {
      
      friends: [
          {
            name: "Kenji",
            age: 33,
            id: 1
          },
          {
            name: "Ottomo",
            age: 46,
            id: 2
          }

        ]
    };


  render() {

    return  (
        <>
        <h1>Hello Galaxy</h1>
        <AddFriend />
        <Friend friends={this.state.friends}/>
        </>
      )
  }
}

export default App;
