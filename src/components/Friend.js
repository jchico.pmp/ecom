import React from 'react';

class Friend extends React.Component {
	


	render() {

		return(
			<div className="bg-warning">
				<ul>
					
						{
							this.props.friends.map( (friend) => {
							return (

							<li>
								My Friend {friend.name} is {friend.age} years old
								<button > &times; </button>
							</li>

							)

						})
						}
						
					
				</ul>
			</div>
			)
	}
}

export default Friend;